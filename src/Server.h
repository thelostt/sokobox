#pragma once

#include <Default.h>
#include "Network.h"

namespace game
{
    class Server : public Network
    {
    public:
        Server();
        ~Server();

        virtual Status bind(unsigned short port);
        virtual Status send()
    };
}