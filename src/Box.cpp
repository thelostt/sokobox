#include "Box.h"

namespace game
{
    Box::Box()
    {}

    Box::Box(const Vector2f& pos, const Color& color)
        : pos(pos), scenePos(Vector2i(0, 0)), size(Vector2f(50, 50)), color(color), color2(color)
    {}

    Box::Box(const Vector2f& pos, const Vector2f& size, const Color& color)
        : pos(pos), scenePos(Vector2i(0, 0)), size(size), color(color), color2(color)
    {}

    Box::Box(const Vector2f& pos, const Color& color, const string& texturePath)
        : Box(pos, Vector2f(50, 50), color)
    {
        getTexture().push(texturePath);
    }

    Box::~Box()
    {}

    Vector2f& Box::getPos()
    {
        return pos;
    }

    Vector2i& Box::getScenePos()
    {
        return scenePos;
    }

    Vector2f& Box::getSize()
    {
        return size;
    }

    Color& Box::getColor()
    {
        return color;
    }

    Color& Box::getSecColor()
    {
        return color2;
    }

    ResManTex& Box::getTexture()
    {
        return texture;
    }

    void Box::operator()(sf::RenderWindow& window)
    {
        sf::Vertex vertices[] =
        {
            sf::Vertex(sf::Vector2f( pos.x,         pos.y),          color,  sf::Vector2f( 0, 0)),
            sf::Vertex(sf::Vector2f( pos.x,         size.y + pos.y), color2, sf::Vector2f( 0, 1)),
            sf::Vertex(sf::Vector2f(size.x + pos.x, size.y + pos.y), color2, sf::Vector2f(1, 1)),
            sf::Vertex(sf::Vector2f(size.x + pos.x, pos.y),          color,  sf::Vector2f(1, 0))
        };

        texture.bind();
        window.draw(vertices, 4, sf::Quads);
        texture.unbind();
    }
}