#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <memory>
#include <functional>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

// For ini files parser
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

extern sf::Clock globalClock;

extern double deltaTime;
extern double oldFrameStart;
extern double frameStart;

namespace game
{
    using std::string;
    using std::vector;
    using sf::Vector2f;
    using sf::Vector2i;
    using sf::Vector2u;
    using sf::Color;
    using sf::Texture;

    using sf::Int64;
    using sf::Int32;
    using sf::Int16;
    using sf::Int8;

    using sf::Uint64;
    using sf::Uint32;
    using sf::Uint16;
    using sf::Uint8;

    inline bool isTimePassed(double delay, double frameTime)
    {
        return frameTime + delay < frameStart;
    }

    inline double getNextFrame(double delay)
    {
        return frameStart + delay;
    }

    inline int hash(const string& word)
    {
        int result = 0, i = 0;
        for(auto& ch : word)
            result += ch * pow(31, ++i);

        return result;
    }
}