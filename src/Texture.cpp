#include "Texture.h"

namespace game
{
    ResManTex::ResManTex()
        : binding(0)
    {}

    ResManTex::~ResManTex()
    {}

    void ResManTex::push(const string& filename)
    {
        Texture texture;
        texture.loadFromFile(filename);
        content.emplace_back(texture);
    }

    void ResManTex::bind()
    {
        bind(binding);
    }

    void ResManTex::bind(int index)
    {
        if(index < content.size())
        {
            binding = index;
            Texture::bind(&content[index]);
        }
    }

    void ResManTex::unbind()
    {
        Texture::bind(nullptr);
    }
}