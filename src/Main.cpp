#include "Default.hpp"
#include <SFGUI/SFGUI.hpp>
#include "Player.h"
#include "Scene.h"
//#include "Network.h"

using namespace game;

sf::Clock globalClock;

double deltaTime = 0.0;
double oldFrameStart = 0.0;
double frameStart = 0.0;

int main()
{
    sf::RenderWindow window(sf::VideoMode(950, 550), "Sokobox II");
    window.setVerticalSyncEnabled(true);

    Scene scene;
    scene.init();
    scene.initWindow(window);

    scene.loadFromPath("map");

    while (window.isOpen())
    {
        frameStart = globalClock.getElapsedTime().asMicroseconds() / 1000000.0;
        deltaTime = frameStart - oldFrameStart;
        oldFrameStart = frameStart;

        sf::Event event;
        window.pollEvent(event); // fuck the rest of events
        switch(event.type)
        {
        case sf::Event::Closed:
            window.close();
            break;

        case sf::Event::LostFocus:
            while(window.pollEvent(event), event.type == sf::Event::GainedFocus);
            break;

        default:
            scene.updateCamera(window, deltaTime);
            scene.update();
            window.clear();
            scene.drawWholeMap(window);
            window.display();
            break;
        }
    }

    return 0;
}