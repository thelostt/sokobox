#pragma once

#include "Default.hpp"
#include "Texture.h"
#include "Box.h"

namespace game
{
    // ============
    //   Direction
    // ============

    enum class Direction
    {
        None,
        Left,
        Top,
        Right,
        Bottom
    };

    // ============
    //   Delegates
    // ============

    class BehaviourDelegate
    {
    public:
        virtual Direction pollBehaviour() = 0;
        virtual ~BehaviourDelegate() {};
    };

    class KeyboardInput : public BehaviourDelegate
    {
    public:
        virtual Direction pollBehaviour();

        KeyboardInput();
    
    protected:
        double m_delayMove;
        double m_frameMove;
    };

    // TODO Artificial intelligent
    class ArtificialInput : public BehaviourDelegate
    {
    public:
        virtual Direction pollBehaviour();
    };

    class NoInput : public BehaviourDelegate
    {
    public:
        virtual Direction pollBehaviour();
    };

    // ============
    //   Character Base
    // ============

    class Player : public Box
    {
    public:
        Player(const Player&) = delete;
        Player(Player&&) = delete;

        Player(std::unique_ptr<BehaviourDelegate> d_behaviour);
        Player(const Vector2i& pos, const string& customIni, std::unique_ptr<BehaviourDelegate> d_behaviour);
        ~Player();

        // Calls d_behaviour->pollBehaviour()
        Direction pollBehaviour();

        void setBehaviour(std::unique_ptr<BehaviourDelegate> d_behaviour);
        void setPosition(const Vector2i& position);
        void updateAnimation();

        unsigned int counter;

    protected:
        std::unique_ptr<BehaviourDelegate> d_behaviour;
        double m_delayAnim;
        Vector2f m_futurePos;

        void init();
    };

}