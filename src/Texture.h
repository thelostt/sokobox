#pragma once

#include "Default.hpp"

namespace game
{
    class ResManTex
    {
        vector<Texture> content;
        int binding;

    public:
        ResManTex();
        ~ResManTex();

        void push(const string& filename);
        void bind();
        void bind(int index);
        void unbind();
    };
}