#include "Default.hpp"
#include "Box.h"
#include "Player.h"
#include "Network.hpp"

const int sokoHash = game::hash("Sokobox II");

sf::Packet& operator >>(sf::Packet& packet, game::Network::Header& header)
{
    return packet >> header.magic >> header.id >> header.ack;
}

sf::Packet& operator <<(sf::Packet& packet, const game::Network::Header& header)
{
    return packet << header.magic << header.id << header.ack;
}

namespace game
{
    // If the variable gets to dataflow, the 0, 1 or even 2 are bigger than the max number of that variable
    //
    //  Ex:
    //   char => ... < 253 < 254 < 255 < 0 < 1 < 2 < ...
    bool sequence_more_recent(unsigned int s1, unsigned int s2, unsigned int max)
    {
        return 
            (( s1 > s2 ) && 
            ( s1 - s2 <= max / 2 ))
               ||
            (( s2 > s1 ) &&
            ( s2 - s1  > max / 2 ));
    }

    Network::Network()
        : m_localSequence(0), m_remoteSequence(0)
    {}

    Network::~Network()
    {}

    bool Network::isConnected()
    {
        return m_connectionStatus == Network::Status::Connected;
    }

    sf::Packet Network::getHeaderPacket(bool isImportant)
    {
        sf::Packet packet;
        Header header;

        // Just set my magic number to the packet
        header.magic = sokoHash;

        // Since it's a new packet, then increase the local sequence
        header.id = ++m_localSequence;

        //  And sets the same ack sequence whether the packet isn't important,
        // so that the other side knows whether the packet is, or is not, important, just looking at
        // the sequence and comparing with the local ack sequence to verify if it's equals or not.
        //  If equals, then it's not a important packet.
        //  If it's different, then it's a important packet.
        header.ack = isImportant? ++m_ackSequence : m_ackSequence;

        // Put all and good luck
        return packet << header;
    }

    boost::optional<sf::Packet> Network::receivePacket()
    {
        if(!isConnected())
        {
            // TODO Should be replaced by a sfgui window
            std::cout << "No connection\n";

            return boost::none;
        }

        sf::Packet packet;

        sf::IpAddress ipSender;
        Uint16 portSender;

        // Get the address and port of the sender
        m_socket.receive(packet, ipSender, portSender);

        // And compare both to check whether it's the espected sender
        if(m_remoteAddress != ipSender || m_remotePort != portSender)
        {
            // If isn't, that's not a packet I would handle
            return boost::none;
        }

        Header header;

        // Now check the magic number
        packet >> header.magic;

        if(header.magic != sokoHash)
        {
            // Okay, not a packet I would handle too
            return boost::none;
        }

        packet >> header.id;
        packet >> header.ack;


        return packet;
    }
    

    // TODO: Ok, there's connection, so do stuffs
    //
    //  * Receives the packet
    //  * Checks if the sender and port is the same
    //  * Receives the packet and check if it's valid packet (checking magic number)
    //  * Sets its content to a temporary package
    //  * Checks its sequence number
    //  * Checks its acknowledgment
    //  * If all is okay, the god exists
    //    + Updates the sequence number
    //    + Sets OK to the ack
}