
all:
	clang++ -std=c++14 src/*.cpp -lsfgui -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system -o bin/sokobox

windows:
	i686-w64-mingw32-g++ -std=c++14 src/*.cpp -lsfgui -lsfml-graphics -lsfml-network  -lsfml-window -lsfml-system -o win32/sokobox.exe

run:
	cd bin && ./sokobox