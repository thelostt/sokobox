#pragma once

#include "Default.hpp"
#include <boost/optional.hpp>

// NOTE: Use a hash of sokobox name for packets id protocol to valid packets

/*

 The acknowlegment system:

TODO
    - Sends the package with the sequence number and its ack (remote sequence)
    - Receives the packet in the other side and checks its sequence number.
      + If sequence number is bigger than my local sequence, set it to me. Else, ignore this packet
        + Checks its ack number and sends it back

    + Receives the ack and checks if it is received as well (if the ack corresponds to my current ack)

*/

namespace game
{
    class Network
    {
    public:
        Network();
        ~Network();

        // Connection status
        enum class Status
        {
            Connected,
            NotConnected,

            // Waiting for a connection
            Listening,
        };

        // Sets its situation when received a packet
        // NOTE: I'M NOT USING THIS ACTUALLY
        enum class PacketSituation
        {
            // Ok, just a normal packet
            Fine,

            // Wow, this is such a important packet. Please, tell the other side you received it
            Relevant,

            // Too old packet
            Old,

            // Not interesing packet. It hasn't the magic number or doesn't correspond to some specs
            NotInteresting,
        };

        // The Packet Header

        /*  Hash / magic number,
         *  Local/Remote sequence,
         *  Acknowlegment,
         *  List of protocol and its content
         */

        struct Header
        {
            // Magic number
            Int32 magic;

            // Local/Remote sequence
            Uint32 id;

            // Acknowlegment
            Uint32 ack;
        };

        bool isConnected();

        sf::Packet getHeaderPacket(bool isImportant);

        boost::optional<sf::Packet> receivePacket();

    protected:
        sf::UdpSocket m_socket;

        // Sending to address
        sf::IpAddress m_localAddress;

        // Receiving from address
        sf::IpAddress m_remoteAddress;

        Uint16 m_localPort;
        Uint16 m_remotePort;

        // Local sequence for the most recent sent packet
        Uint32 m_localSequence;

        // Remote sequence for the most recent received packet
        Uint32 m_remoteSequence;

        // Acknowlegment sequence
        Uint32 m_ackSequence;

        vector<sf::Packet> m_pendingAcks;

        // Connection status
        Network::Status m_connectionStatus;
    };
}