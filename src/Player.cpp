#include "Player.h"
#include "Scene.h"

namespace game
{
    // ============
    //   Delegates
    // ============

    KeyboardInput::KeyboardInput()
        : m_delayMove(0.06)
    {
        m_frameMove = getNextFrame(m_delayMove);
    }

    Direction KeyboardInput::pollBehaviour()
    {
        if(isTimePassed(m_delayMove, m_frameMove))
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                m_frameMove = getNextFrame(m_delayMove);

                return sf::Keyboard::isKeyPressed(sf::Keyboard::W)? Direction::Top : Direction::Bottom;
            }
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                m_frameMove = getNextFrame(m_delayMove);
                
                return sf::Keyboard::isKeyPressed(sf::Keyboard::A)? Direction::Left : Direction::Right;
            }
            
        }
        
        return Direction::None;
    }

    Direction ArtificialInput::pollBehaviour()
    {
        switch(rand() % 50)
        {
        case 0:
            return Direction::Left;
        case 1:
            return Direction::Right;
        case 2:
            return Direction::Top;
        case 3:
            return Direction::Bottom;
        default:
            break;
        }   
        return Direction::None;
    }

    Direction NoInput::pollBehaviour()
    {
        return Direction::None;
    }

    // ============
    //   Character Base
    // ============

    Player::Player(std::unique_ptr<BehaviourDelegate> d_b)
        : Box(Vector2f(0, 0), Color::White)
    {
        setBehaviour(std::move(d_b));
        init();
    }

    Player::Player(const Vector2i& pos, const string& customIni, std::unique_ptr<BehaviourDelegate> d_b)
        : Box(Vector2f(0, 0), Color::White)
    {
        this->pos = Vector2f(pos.x * 50, pos.y * 50);
        setPosition(pos);

        setBehaviour(std::move(d_b));

        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini(customIni, pt);
        
        texture.push(pt.get<std::string>("Player.TexturePath"));

        getColor() = Color(pt.get<Uint8>("Player.RedPrimColor"),
                           pt.get<Uint8>("Player.GreenPrimColor"),
                           pt.get<Uint8>("Player.BluePrimColor"), 255);

        getSecColor() = Color(pt.get<Uint8>("Player.RedSecColor"),
                              pt.get<Uint8>("Player.GreenSecColor"),
                              pt.get<Uint8>("Player.BlueSecColor"), 255);

        init();
    }

    Player::~Player()
    {}

    Direction Player::pollBehaviour()
    {
        return d_behaviour->pollBehaviour();
    }

    void Player::setBehaviour(std::unique_ptr<BehaviourDelegate> d_b)
    {
        d_behaviour = std::move(d_b);
    }

    void Player::setPosition(const Vector2i& position)
    {
        scenePos = position;
        m_futurePos = Vector2f(position.x * 50, position.y * 50);
    }

    void Player::updateAnimation()
    {
        if(m_futurePos.x > pos.x + 10.0f || m_futurePos.x < pos.x - 10.0f)
            pos.x += (fabs(m_futurePos.x - pos.x) / (m_futurePos.x - pos.x)) * deltaTime * m_delayAnim;
        else pos.x = m_futurePos.x;
        
        if(m_futurePos.y > pos.y + 10.0f || m_futurePos.y < pos.y - 10.0f)
            pos.y += (fabs(m_futurePos.y - pos.y) / (m_futurePos.y - pos.y)) * deltaTime * m_delayAnim;
        else pos.y = m_futurePos.y;
    }

    void Player::init()
    {
        m_delayAnim = 550.0f;
        counter = 0;
    }
}