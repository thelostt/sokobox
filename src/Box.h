#pragma once

#include "Default.hpp"
#include "Texture.h"

namespace game
{
    class Box
    {
    protected:
        Vector2f pos;
        Vector2i scenePos;
        Vector2f size;
        Color color;
        Color color2;
        ResManTex texture;

        // Constructor with more parameters
        Box(const Vector2f& pos, const Vector2f& size, const Color& color);

    public:
        Box(const Box&) = delete;
        Box(Box&&) = default;

        Box();
        Box(const Vector2f& pos, const Color& color);
        Box(const Vector2f& pos, const Color& color, const string& texturePath);
        ~Box();

        Vector2f& getPos();
        Vector2i& getScenePos();
        Vector2f& getSize();

        Color& getColor();
        Color& getSecColor();

        ResManTex& getTexture();

        // Draw me on window
        void operator()(sf::RenderWindow& w);
    };
}