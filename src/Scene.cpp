#include "Scene.h"
#include "Player.h"
#include <cstdint>

namespace game
{
    const Scene::BoxSpec Scene::emptyBlock = BoxSpec(BlockBehaviour::Invisible,   BlockId::Empty);
    const Scene::BoxSpec Scene::boxBlock   = BoxSpec(BlockBehaviour::Interactive, BlockId::Box);

    Scene::Scene()
    {}

    Scene::~Scene()
    {}

    void Scene::loadFromPath(const string& path)
    {
        sf::Image ground;
        sf::Image points;
        sf::Image boxes;
        sf::Image front;
        sf::Image npcs;

        ground.loadFromFile(path + "/ground.png");
        points.loadFromFile(path + "/points.png");
        boxes.loadFromFile(path + "/boxes.png");
        front.loadFromFile(path + "/front.png");
        npcs.loadFromFile(path + "/players.png");

        map.ground.clear();
        map.points.clear();
        map.boxes.clear();
        map.front.clear();
        map.players.clear();
        map.size = front.getSize();

        // LOAD GROUND
        for(int x = 0; x < map.size.x; ++x)
        {
            vector<BoxSpec> b;

            for(int y = 0; y < map.size.y; ++y)
            {
                Color pixel = ground.getPixel(x, y);

                if(pixel == Color::White)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Wall)); // Wall
                
                else if(pixel == Color::Blue)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::BackWall)); // Untouchable wall

                else if(pixel == Color::Red)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Box)); // Box

                else if(pixel == Color::Green)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Point)); // Point

                else
                    b.emplace_back(emptyBlock); // Nothing, just empty space
            }

            map.ground.emplace_back(b);
        }

        // LOAD POINTS
        for(int x = 0; x < map.size.x; ++x)
        {
            vector<BoxSpec> b;

            for(int y = 0; y < map.size.y; ++y)
            {
                Color pixel = points.getPixel(x, y);

                if(pixel == Color::White)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Point)); // Point

                else
                    b.emplace_back(emptyBlock); // Nothing, just empty space
            }

            map.points.emplace_back(b);
        }

        // LOAD BOXES
        for(int x = 0; x < map.size.x; ++x)
        {
            vector<BoxSpec> b;

            for(int y = 0; y < map.size.y; ++y)
            {
                Color pixel = boxes.getPixel(x, y);

                if(pixel == Color::White)
                    b.emplace_back(BoxSpec(BlockBehaviour::Interactive, BlockId::Box)); // Box

                else if(pixel == Color::Red)
                    b.emplace_back(BoxSpec(BlockBehaviour::Interactive, BlockId::Wall)); // Wall

                else
                    b.emplace_back(emptyBlock); // Nothing, just empty space
            }

            map.boxes.emplace_back(b);
        }

        // LOAD FRONT
        for(int x = 0; x < map.size.x; ++x)
        {
            vector<BoxSpec> b;

            for(int y = 0; y < map.size.y; ++y)
            {
                Color pixel = front.getPixel(x, y);

                if(pixel == Color::White)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Wall)); // Wall
                
                else if(pixel == Color::Blue)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::BackWall)); // Untouchable wall

                else if(pixel == Color::Red)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Box)); // Box

                else if(pixel == Color::Green)
                    b.emplace_back(BoxSpec(BlockBehaviour::Immovable, BlockId::Point)); // Point

                else
                    b.emplace_back(emptyBlock); // Nothing, just empty space
            }

            map.front.emplace_back(b);
        }

        // LOAD PLAYERS
        for(int x = 0; x < map.size.x; ++x)
        {
            for(int y = 0; y < map.size.y; ++y)
            {
                Color pixel = npcs.getPixel(x, y);
                Vector2i position(x, y);

                if(pixel == Color::White)
                {
                    map.players.emplace_back(position, "data/humanPlayer.ini", std::make_unique<KeyboardInput>());
                    setCameraToFollow(map.players.back()); // TODO server and client
                }

                else if(pixel == Color::Blue)
                    map.players.emplace_back(position, "data/enemyPlayer.ini", std::make_unique<ArtificialInput>());

                else if(pixel == Color::Red)
                    map.players.emplace_back(position, "data/humanPlayer.ini", std::make_unique<ArtificialInput>());

                else continue;
            }
        }
    }

    void Scene::saveToFile(const string& path)
    {
        sf::Image ground;
        sf::Image points;
        sf::Image boxes;
        sf::Image front;
        sf::Image npcs;

        // TODO saver

        ground.create(map.size.x, map.size.y, Color::Black);
        points.create(map.size.x, map.size.y, Color::Black);
        boxes.create(map.size.x, map.size.y, Color::Black);
        front.create(map.size.x, map.size.y, Color::Black);
        npcs.create(map.size.x, map.size.y, Color::Black);

        for(int i = 0, x = 0; x < map.size.x; ++x)
        {
            
        }
    }

    void Scene::drawWholeMap(sf::RenderWindow& window)
    {
        drawLayer(map.ground, window);
        drawLayer(map.points, window);
        drawLayer(map.boxes, window);
        drawLayer(map.front, window);

        for(auto& npc : map.players)
        {
            npc.updateAnimation();
            npc(window);
        }
    }

    void Scene::update()
    {
        for(auto& npc : map.players)
            checkInteraction(npc);
    }

    void Scene::setCameraToFollow(Player& npc)
    {
        m_following = &npc;
    }

    void Scene::updateCamera(sf::RenderWindow& window, double deltaTime)
    {
        const float velocity = 1.5;

        auto fabszero = [](float v) -> float
        {
            const float absolute = fabs(v);
            return absolute < 5.0? 0.0 : absolute;
        };

        if(m_following != nullptr)
        {
            if(m_following->getPos().x < 475.f)
                m_camera.setCenter(m_camera.getCenter().x - fabszero(m_camera.getCenter().x - 475.f) * deltaTime * velocity, m_camera.getCenter().y);

            else if(m_following->getPos().x > map.size.x * 50 - 475.f)
                m_camera.setCenter(m_camera.getCenter().x + fabszero(475.f + m_camera.getCenter().x - map.size.x * 50) * deltaTime * velocity, m_camera.getCenter().y);

            else if(m_camera.getCenter().x > m_following->getPos().x + 2 && m_camera.getCenter().x > 475.f)
                m_camera.setCenter(m_camera.getCenter().x - fabszero(m_camera.getCenter().x - m_following->getPos().x) * deltaTime * velocity, m_camera.getCenter().y);

            else if(m_camera.getCenter().x < m_following->getPos().x - 2 && m_camera.getCenter().x < map.size.x * 50 - 475.f)
                m_camera.setCenter(m_camera.getCenter().x + fabszero(m_camera.getCenter().x - m_following->getPos().x) * deltaTime * velocity, m_camera.getCenter().y);
        }
    }

    void Scene::initWindow(sf::RenderWindow& window)
    {
        m_camera = window.getView();
        window.setView(m_camera);
    }

    void Scene::init()
    {
        boxTextures.emplace_back(Vector2f(0, 0), Color::White, "textures/white stuff.png");
        boxTextures.emplace_back(Vector2f(0, 0), Color::White, "textures/dark stuff.png");
        boxTextures.emplace_back(Vector2f(0, 0), Color(255, 200, 200), "textures/red stuff.png");
        boxTextures.emplace_back(Vector2f(0, 0), Color::White, "textures/point stuff.png");
    }

    void Scene::drawLayer(const Layer& layer, sf::RenderWindow& window)
    {
        float xf = 0.f;
        float yf = 0.f;
        for(const auto& x : layer)
        {
            yf = 0.f;
            for(const auto& y : x)
            {
                if(y.second != BlockId::Empty)
                {
                    Vector2f position(xf, yf);
                    if(  position.x > m_camera.getCenter().x + 475.f
                      || position.x < m_camera.getCenter().x - (475.f + 50.f))
                        continue;

                    Box& box = boxTextures[(int)y.second];

                    box.getPos() = position;
                    box(window);
                }
                yf += 50.f;
            }
            xf += 50.f;
        }
    }

    void Scene::checkInteraction(Player& npc)
    {
        Direction behaviour = npc.pollBehaviour();

        if(behaviour == Direction::None)
            return;

        Vector2i future = npc.getScenePos() + directionToCoordinates(behaviour);

        if(askToMove(future, behaviour))
        {
            npc.setPosition(future);
        }
    }

    Vector2i Scene::directionToCoordinates(Direction dir)
    {
        Vector2i coordinates(0, 0);

        switch(dir)
        {
        case Direction::Left:
            coordinates.x = -1;
            break;

        case Direction::Right:
            coordinates.x = 1;
            break;

        case Direction::Top:
            coordinates.y = -1;
            break;

        case Direction::Bottom:
            coordinates.y = 1;
            break;

        case Direction::None:
            break;
        }

        return coordinates;
    }

    bool Scene::askToMove(const Vector2i& coord, Direction dir)
    {
        // The next place over coord
        Vector2i fut = coord + directionToCoordinates(dir);

        bool isTherePlayer = false;
        bool futIsTherePlayer = false;
        for(auto& p: map.players)
        {
            isTherePlayer = isTherePlayer? isTherePlayer : coord == p.getScenePos();
            futIsTherePlayer = futIsTherePlayer? futIsTherePlayer : fut == p.getScenePos();
        }

        if( map.boxes[coord.x][coord.y] == emptyBlock
         && map.front[coord.x][coord.y] == emptyBlock
         && !isTherePlayer)
            return true;

        else if( map.boxes[coord.x][coord.y].first == BlockBehaviour::Interactive
              && map.boxes[fut.x][fut.y] == emptyBlock
              && map.front[fut.x][fut.y] == emptyBlock
              && !futIsTherePlayer)
        {
            map.boxes[fut.x][fut.y] = map.boxes[coord.x][coord.y];
            map.boxes[coord.x][coord.y] = emptyBlock;
            return true;
        }

        return false;
    }
}