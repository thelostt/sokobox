#pragma once

#include "Default.hpp"
#include "Box.h"
#include "Player.h"
//#include "Network.h"

namespace game
{
    using std::pair;


    class Player;
    class HumanBehaviour;
    class EnemyBehaviour;

    class Scene
    {
    public:
        enum class BlockBehaviour
        {
            Invisible,
            Point,
            Immovable,
            Interactive,
        };

        enum class BlockId
        {
            Wall,
            BackWall,
            Box,
            Point,
            Empty
        };

        using BoxSpec = pair<BlockBehaviour, BlockId>;
        using Layer = vector<vector<BoxSpec>>;

        static const BoxSpec emptyBlock;
        static const BoxSpec boxBlock;

        struct Map
        {
            Layer ground;
            Layer points;
            Layer boxes;
            Layer front;
            
            std::list<Player> players;

            sf::Vector2u size;
        };

        Scene();
        ~Scene();

        // loads a map and stack it
        void loadFromPath(const string& path);

        // Saves the actual map to a file
        void saveToFile(const string& path);

        // draws the whole map
        void drawWholeMap(sf::RenderWindow& window);

        // deal with players, enemies and other stuff
        void update();

        // set the camera to follow some living block
        void setCameraToFollow(Player& npc);

        // update camera
        void updateCamera(sf::RenderWindow& window, double deltaTime);

        // load box textures
        void init();

        // sets some deal with window
        void initWindow(sf::RenderWindow& window);

    protected:
        // ======
        //  Map and Level
        Map map;
        vector<Box> boxTextures;
        int playingLevelIndex;

        // ======
        //  Camera
        sf::View m_camera;
        float m_cameraDelay;
        Player* m_following;

        // ======
        //  Living blocks interaction
        void drawLayer(const Layer& layer, sf::RenderWindow& window);
        void checkInteraction(Player& npc);

        Vector2i directionToCoordinates(Direction dir);

        bool askToMove(const Vector2i& coordinates, Direction dir);
    };
}