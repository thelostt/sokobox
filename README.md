# Sokobox
A sokobox based game with more action. This is an old project I'm remaking.

About
=====
The game is only multiplayer, with maps and whatever only for multiplayer gaming. The objective is put the red boxes on the points. Who played sokobox knows how it is.

However, there are enemies protecting these boxes. If some enemy see you, game over. If the enemies see the boxes in "wrong place", they bring back these ones to its original place.

For now, that's it. I'll finish the game and some other stuffs.
